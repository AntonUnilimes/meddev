const state = {
	tasks: {
		'ID1': {
			name: 'Go to shop',
			completed: false,
			dueDate: '2019/05/12',
			dueTime: '18:30'
		},
		'ID2': {
			name: 'Get bananas',
			completed: false,
			dueDate: '2019/05/13',
			dueTime: '14:00'
		},
		'ID3': {
			name: 'Get apples',
			completed: false,
			dueDate: '2019/05/14',
			dueTime: '16:00'
		}	
	},
	info: "vuex test"
}

const mutations = {
	updateInfo(state, payload){
		state.info = payload
	}
}

const actions = {
	
}

const getters = {
	tasks: (state) => {
		return state.tasks
	},
	info: (state) => {
		console.log(222, state)
		return 333; //state.info
	}
}

export default {
	namespaced: true,
	state,
	mutations,
	actions,
	getters
}