import Vue from 'vue'
import Vuex from 'vuex'

import tasks from './store-tasks'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function (/* { ssrContext } */) {
  // const Store = new Vuex.Store({
  //   modules: {
  //     tasks
  //   },

  //   // enable strict mode (adds overhead!)
  //   // for dev mode only
  //   strict: process.env.DEV
  // })

  const Store = new Vuex.Store({
   state: {
     selectedPatient: {},
     systolicBP: '',
     diastolicBP: '',
     option1: '',
     option2: '',
     croppedImg: "",
     croppedPlanes: []
   },
   mutations: {
     selectPatient (state, payload){
       state.selectedPatient = payload.patient
     },
     selectCropp (state, payload){
       state.croppedImg = "" + payload.dataUrl
     },
     setSystolicBP (state, systolicBP){
      state.systolicBP = systolicBP
     },
     setDiastolicBP (state, diastolicBP){
      state.diastolicBP = diastolicBP
     },
     setOption1 (state, option1) {
      state.option1 = option1
     },
     setOption2 (state, option2) {
      state.option2 = option2
     },
     setCropp3dModel (state, planes) {
      state.croppedPlanes = planes
     },
   },
   actions: {
     setSystolicBP ({commit}, systolicBP){
      commit('setSystolicBP', systolicBP)
     },
     setDiastolicBP ({commit}, diastolicBP){
      commit('setDiastolicBP', diastolicBP)
     },
     setOption1 ({commit}, option1) {
      commit('setOption1', option1)
     },
     setOption2 ({commit}, option2) {
      commit('setOption2', option2)
     },
     setCropp3dModel ({commit}, planes){
      commit('setCropp3dModel', planes)
     }
   }
  })

  return Store
}
